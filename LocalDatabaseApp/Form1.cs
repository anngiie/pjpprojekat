﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LocalDatabaseApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void playerScoreBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.playerScoreBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.playersDataSet1);

        }

        private void playerScoreBindingNavigatorSaveItem_Click_1(object sender, EventArgs e)
        {
            this.Validate();
            this.playerScoreBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.playersDataSet1);

        }

        private void playerScoreBindingNavigatorSaveItem_Click_2(object sender, EventArgs e)
        {
            this.Validate();
            this.playerScoreBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.playersDataSet1);

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'playersDataSet1.PlayerScore' table. You can move, or remove it, as needed.
            this.playerScoreTableAdapter.Fill(this.playersDataSet1.PlayerScore);

        }

        private void scoreLabel_Click(object sender, EventArgs e)
        {

        }

        private void game_ModeLabel_Click(object sender, EventArgs e)
        {

        }

        private void iDLabel_Click(object sender, EventArgs e)
        {

        }

        private void nameLabel_Click(object sender, EventArgs e)
        {

        }

        private void surnameLabel_Click(object sender, EventArgs e)
        {

        }

        private void playerScoreDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void iDTextBox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
