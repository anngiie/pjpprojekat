﻿namespace LocalDatabaseApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label iDLabel;
            System.Windows.Forms.Label nameLabel;
            System.Windows.Forms.Label surnameLabel;
            System.Windows.Forms.Label scoreLabel;
            System.Windows.Forms.Label game_ModeLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.playersDataSet1 = new LocalDatabaseApp.PlayersDataSet1();
            this.playerScoreBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.playerScoreTableAdapter = new LocalDatabaseApp.PlayersDataSet1TableAdapters.PlayerScoreTableAdapter();
            this.tableAdapterManager = new LocalDatabaseApp.PlayersDataSet1TableAdapters.TableAdapterManager();
            this.playerScoreBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.playerScoreBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.iDTextBox = new System.Windows.Forms.TextBox();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.surnameTextBox = new System.Windows.Forms.TextBox();
            this.scoreTextBox = new System.Windows.Forms.TextBox();
            this.game_ModeCheckBox = new System.Windows.Forms.CheckBox();
            this.playerScoreDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            iDLabel = new System.Windows.Forms.Label();
            nameLabel = new System.Windows.Forms.Label();
            surnameLabel = new System.Windows.Forms.Label();
            scoreLabel = new System.Windows.Forms.Label();
            game_ModeLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.playersDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerScoreBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerScoreBindingNavigator)).BeginInit();
            this.playerScoreBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.playerScoreDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // iDLabel
            // 
            iDLabel.AutoSize = true;
            iDLabel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            iDLabel.Font = new System.Drawing.Font("Agency FB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            iDLabel.ForeColor = System.Drawing.Color.Black;
            iDLabel.Location = new System.Drawing.Point(36, 110);
            iDLabel.Name = "iDLabel";
            iDLabel.Size = new System.Drawing.Size(23, 20);
            iDLabel.TabIndex = 1;
            iDLabel.Text = "ID:";
            iDLabel.Click += new System.EventHandler(this.iDLabel_Click);
            // 
            // nameLabel
            // 
            nameLabel.AutoSize = true;
            nameLabel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            nameLabel.Font = new System.Drawing.Font("Agency FB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            nameLabel.ForeColor = System.Drawing.Color.Black;
            nameLabel.Location = new System.Drawing.Point(36, 140);
            nameLabel.Name = "nameLabel";
            nameLabel.Size = new System.Drawing.Size(41, 20);
            nameLabel.TabIndex = 3;
            nameLabel.Text = "Name:";
            nameLabel.Click += new System.EventHandler(this.nameLabel_Click);
            // 
            // surnameLabel
            // 
            surnameLabel.AutoSize = true;
            surnameLabel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            surnameLabel.Font = new System.Drawing.Font("Agency FB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            surnameLabel.ForeColor = System.Drawing.Color.Black;
            surnameLabel.Location = new System.Drawing.Point(36, 170);
            surnameLabel.Name = "surnameLabel";
            surnameLabel.Size = new System.Drawing.Size(58, 20);
            surnameLabel.TabIndex = 5;
            surnameLabel.Text = "Surname:";
            surnameLabel.Click += new System.EventHandler(this.surnameLabel_Click);
            // 
            // scoreLabel
            // 
            scoreLabel.AutoSize = true;
            scoreLabel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            scoreLabel.Font = new System.Drawing.Font("Agency FB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            scoreLabel.ForeColor = System.Drawing.Color.Black;
            scoreLabel.Location = new System.Drawing.Point(36, 200);
            scoreLabel.Name = "scoreLabel";
            scoreLabel.Size = new System.Drawing.Size(43, 20);
            scoreLabel.TabIndex = 7;
            scoreLabel.Text = "Score:";
            scoreLabel.Click += new System.EventHandler(this.scoreLabel_Click);
            // 
            // game_ModeLabel
            // 
            game_ModeLabel.AutoSize = true;
            game_ModeLabel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            game_ModeLabel.Font = new System.Drawing.Font("Agency FB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            game_ModeLabel.ForeColor = System.Drawing.Color.Black;
            game_ModeLabel.Location = new System.Drawing.Point(36, 232);
            game_ModeLabel.Name = "game_ModeLabel";
            game_ModeLabel.Size = new System.Drawing.Size(72, 20);
            game_ModeLabel.TabIndex = 9;
            game_ModeLabel.Text = "Game-Mode:";
            game_ModeLabel.Click += new System.EventHandler(this.game_ModeLabel_Click);
            // 
            // playersDataSet1
            // 
            this.playersDataSet1.DataSetName = "PlayersDataSet1";
            this.playersDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // playerScoreBindingSource
            // 
            this.playerScoreBindingSource.DataMember = "PlayerScore";
            this.playerScoreBindingSource.DataSource = this.playersDataSet1;
            // 
            // playerScoreTableAdapter
            // 
            this.playerScoreTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.PlayerScoreTableAdapter = this.playerScoreTableAdapter;
            this.tableAdapterManager.UpdateOrder = LocalDatabaseApp.PlayersDataSet1TableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // playerScoreBindingNavigator
            // 
            this.playerScoreBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.playerScoreBindingNavigator.BindingSource = this.playerScoreBindingSource;
            this.playerScoreBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.playerScoreBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.playerScoreBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.playerScoreBindingNavigatorSaveItem});
            this.playerScoreBindingNavigator.Location = new System.Drawing.Point(15, 0);
            this.playerScoreBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.playerScoreBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.playerScoreBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.playerScoreBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.playerScoreBindingNavigator.Name = "playerScoreBindingNavigator";
            this.playerScoreBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.playerScoreBindingNavigator.Size = new System.Drawing.Size(1005, 25);
            this.playerScoreBindingNavigator.TabIndex = 0;
            this.playerScoreBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // playerScoreBindingNavigatorSaveItem
            // 
            this.playerScoreBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.playerScoreBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("playerScoreBindingNavigatorSaveItem.Image")));
            this.playerScoreBindingNavigatorSaveItem.Name = "playerScoreBindingNavigatorSaveItem";
            this.playerScoreBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.playerScoreBindingNavigatorSaveItem.Text = "Save Data";
            this.playerScoreBindingNavigatorSaveItem.Click += new System.EventHandler(this.playerScoreBindingNavigatorSaveItem_Click_2);
            // 
            // iDTextBox
            // 
            this.iDTextBox.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.iDTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.iDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playerScoreBindingSource, "ID", true));
            this.iDTextBox.Font = new System.Drawing.Font("Agency FB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iDTextBox.ForeColor = System.Drawing.Color.Black;
            this.iDTextBox.Location = new System.Drawing.Point(123, 107);
            this.iDTextBox.Name = "iDTextBox";
            this.iDTextBox.Size = new System.Drawing.Size(104, 20);
            this.iDTextBox.TabIndex = 2;
            this.iDTextBox.TextChanged += new System.EventHandler(this.iDTextBox_TextChanged);
            // 
            // nameTextBox
            // 
            this.nameTextBox.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.nameTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.nameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playerScoreBindingSource, "Name", true));
            this.nameTextBox.Font = new System.Drawing.Font("Agency FB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameTextBox.ForeColor = System.Drawing.Color.Black;
            this.nameTextBox.Location = new System.Drawing.Point(123, 137);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(104, 20);
            this.nameTextBox.TabIndex = 4;
            // 
            // surnameTextBox
            // 
            this.surnameTextBox.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.surnameTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.surnameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playerScoreBindingSource, "Surname", true));
            this.surnameTextBox.Font = new System.Drawing.Font("Agency FB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.surnameTextBox.ForeColor = System.Drawing.Color.Black;
            this.surnameTextBox.Location = new System.Drawing.Point(123, 167);
            this.surnameTextBox.Name = "surnameTextBox";
            this.surnameTextBox.Size = new System.Drawing.Size(104, 20);
            this.surnameTextBox.TabIndex = 6;
            // 
            // scoreTextBox
            // 
            this.scoreTextBox.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.scoreTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.scoreTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.playerScoreBindingSource, "Score", true));
            this.scoreTextBox.Font = new System.Drawing.Font("Agency FB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.scoreTextBox.ForeColor = System.Drawing.Color.Black;
            this.scoreTextBox.Location = new System.Drawing.Point(123, 197);
            this.scoreTextBox.Name = "scoreTextBox";
            this.scoreTextBox.Size = new System.Drawing.Size(104, 20);
            this.scoreTextBox.TabIndex = 8;
            // 
            // game_ModeCheckBox
            // 
            this.game_ModeCheckBox.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.game_ModeCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.playerScoreBindingSource, "Game-Mode", true));
            this.game_ModeCheckBox.Font = new System.Drawing.Font("Agency FB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.game_ModeCheckBox.ForeColor = System.Drawing.Color.Black;
            this.game_ModeCheckBox.Location = new System.Drawing.Point(123, 227);
            this.game_ModeCheckBox.Name = "game_ModeCheckBox";
            this.game_ModeCheckBox.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.game_ModeCheckBox.Size = new System.Drawing.Size(104, 24);
            this.game_ModeCheckBox.TabIndex = 10;
            this.game_ModeCheckBox.Text = "Campaign";
            this.game_ModeCheckBox.UseVisualStyleBackColor = false;
            // 
            // playerScoreDataGridView
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.playerScoreDataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.playerScoreDataGridView.AutoGenerateColumns = false;
            this.playerScoreDataGridView.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.playerScoreDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.playerScoreDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewCheckBoxColumn1});
            this.playerScoreDataGridView.DataSource = this.playerScoreBindingSource;
            this.playerScoreDataGridView.GridColor = System.Drawing.SystemColors.Desktop;
            this.playerScoreDataGridView.Location = new System.Drawing.Point(290, 63);
            this.playerScoreDataGridView.Name = "playerScoreDataGridView";
            this.playerScoreDataGridView.Size = new System.Drawing.Size(543, 351);
            this.playerScoreDataGridView.TabIndex = 12;
            this.playerScoreDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.playerScoreDataGridView_CellContentClick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "ID";
            this.dataGridViewTextBoxColumn1.HeaderText = "ID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Name";
            this.dataGridViewTextBoxColumn2.HeaderText = "Name";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Surname";
            this.dataGridViewTextBoxColumn3.HeaderText = "Surname";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Score";
            this.dataGridViewTextBoxColumn4.HeaderText = "Score";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.DataPropertyName = "Game-Mode";
            this.dataGridViewCheckBoxColumn1.HeaderText = "Game-Mode";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::LocalDatabaseApp.Properties.Resources.awgtqioQ;
            this.ClientSize = new System.Drawing.Size(1020, 538);
            this.Controls.Add(this.playerScoreDataGridView);
            this.Controls.Add(iDLabel);
            this.Controls.Add(this.iDTextBox);
            this.Controls.Add(nameLabel);
            this.Controls.Add(this.nameTextBox);
            this.Controls.Add(surnameLabel);
            this.Controls.Add(this.surnameTextBox);
            this.Controls.Add(scoreLabel);
            this.Controls.Add(this.scoreTextBox);
            this.Controls.Add(game_ModeLabel);
            this.Controls.Add(this.game_ModeCheckBox);
            this.Controls.Add(this.playerScoreBindingNavigator);
            this.ForeColor = System.Drawing.Color.White;
            this.Name = "Form1";
            this.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.playersDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerScoreBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerScoreBindingNavigator)).EndInit();
            this.playerScoreBindingNavigator.ResumeLayout(false);
            this.playerScoreBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.playerScoreDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private PlayersDataSet1 playersDataSet1;
        private System.Windows.Forms.BindingSource playerScoreBindingSource;
        private PlayersDataSet1TableAdapters.PlayerScoreTableAdapter playerScoreTableAdapter;
        private PlayersDataSet1TableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator playerScoreBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton playerScoreBindingNavigatorSaveItem;
        private System.Windows.Forms.TextBox iDTextBox;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.TextBox surnameTextBox;
        private System.Windows.Forms.TextBox scoreTextBox;
        private System.Windows.Forms.CheckBox game_ModeCheckBox;
        private System.Windows.Forms.DataGridView playerScoreDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;


    }
}

